﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Diagnostics;

public class playercontroller : MonoBehaviour
{
    Rigidbody2D rb2D;
    public Animator animator;
    public TextMeshProUGUI countText;
    public GameObject winText;
    public GameObject loseText;
    public GameObject player;
    public float runSpeed;
    public float jumpForce;

    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        SetCountText();

        loseText.SetActive(false);
        winText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= -5)
        {
            player.SetActive(false);

            if (loseText != null)
            {
                loseText.SetActive(true);
                UnityEngine.Debug.Log("You Lost Bucko");
                Destroy(winText);

                Invoke("Reset", 1.0f);
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }

        if (rb2D.velocity.y == 0)
        {
            OnLanding();
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rb2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rb2D.velocity.y);

        animator.SetFloat("Speed", Mathf.Abs(horizontalInput));

        //Flip Character:
        Vector3 characterScale = transform.localScale;
        if (Input.GetAxis("Horizontal") < 0)
        {
            characterScale.x = -1;
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            characterScale.x = 1;
        }

        transform.localScale = characterScale;
    }

    void Jump()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
        animator.SetBool("IsJumping", true);
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 30)
        {
            winText.SetActive(true);
            Invoke("EndGame", 1.0f);
            Destroy(loseText);
        }
    }

    void Reset()
    {
        SceneManager.LoadScene("SampleScene");
    }
    
    void EndGame()
    {
        Application.Quit();
    }
}
